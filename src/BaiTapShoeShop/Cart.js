import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    return (
      <div className="container pt-3">
        <table className="table table-dark">
          <thead>
            <td>Id</td>
            <td>Tên sản phẩm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>

          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      className="btn btn-success"
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, 1);
                      }}
                    >
                      Tăng
                    </button>
                    <span className="mx-3">{item?.soLuong}</span>
                    <button
                      className="btn btn-secondary"
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, -1);
                      }}
                    >
                      Giảm
                    </button>
                  </td>

                  <td>
                    <button
                      onClick={() => {
                        this.props.handleRemoveSanPham(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
