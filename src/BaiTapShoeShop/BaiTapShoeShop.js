import React, { Component } from "react";
import Cart from "./Cart";
import { DataShoeShop } from "./DataShoeShop";
import ProductList from "./ProductList";

export default class BaiTapShoeShop extends Component {
  state = {
    productList: DataShoeShop,
    gioHang: [],
  };
  handleAddToCart = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = cloneGioHang.findIndex((item) => {
      return item.id === sanPham.id;
    });

    if (index === -1) {
      // Sản phẩm được thêm vào chưa có trong giỏ hàng
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      // Sản phẩm được thêm vào đã có trong giỏ hàng

      cloneGioHang[index].soLuong++;
    }
    // cloneGioHang.push(sanPham);

    this.setState({ gioHang: cloneGioHang });
  };

  handleTangGiamSoLuong = (idSanPham, giaTri) => {
    let cloneGioiHang = [...this.state.gioHang];
    let index = cloneGioiHang.findIndex((item) => {
      return item.id === idSanPham;
    });
    if (index !== -1) {
      cloneGioiHang[index].soLuong += giaTri;
    }

    cloneGioiHang[index].soLuong === 0 && cloneGioiHang.splice(index, 1);

    this.setState({ gioHang: cloneGioiHang });
  };

  handleRemoveSanPham = (idSanPham) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idSanPham;
    });

    if (index !== -1) {
      // let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  render() {
    return (
      <div>
        <h3 className="pt-4 text-danger">Shoe Shop</h3>
        <ProductList
          handleThemSanPham={this.handleAddToCart}
          productList={this.state.productList}
        />

        <h4 className="pt-4">
          Số lượng sản phẩm trong giỏ hàng: {this.state.gioHang.length}
        </h4>
        {this.state.gioHang.length > 0 && (
          <Cart
            handleRemoveSanPham={this.handleRemoveSanPham}
            gioHang={this.state.gioHang}
            handleTangGiamSoLuong={this.handleTangGiamSoLuong}
          />
        )}

        <br />
      </div>
    );
  }
}
