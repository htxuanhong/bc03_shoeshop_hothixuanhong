import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="card col-3   ">
        <img src={image} className="card-img-top" alt="" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
        </div>
        <div className="pb-4">
          <button
            onClick={() => {
              this.props.handleThemSanPham(this.props.data);
            }}
            className="btn btn-info"
          >
            Add to card
          </button>
        </div>
      </div>
    );
  }
}
