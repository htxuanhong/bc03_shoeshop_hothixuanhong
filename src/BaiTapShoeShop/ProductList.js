import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container py-4">
        <div className="row">
          {this.props.productList.map((item, index) => {
            return (
              <ProductItem
                handleThemSanPham={this.props.handleThemSanPham}
                data={item}
                key={index}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
